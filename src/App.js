import React from 'react';
import Layout from './hoc/Layout/Layout'
import { Route, Switch } from 'react-router-dom';
import HomePage from './Container/homepage/homepage';
import OurTeam from './Container/ourTeam/ourTeam';
import Gallery from './Container/gallery/gallery'
import Packages from './Container/packages/packages'
import SocialWork from './Container/socialWorks/socialWorks'
import  ContactUs from './Container/contactUs/contactUs'
import About from './Container/about/about';
import Work from './Container/ourWork/ourWork'


function App() {
  return (
    <Layout >
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/about" exact component={About} />
        <Route path="/ourWork" exact component={Work} />

        <Route path="/contact" exact component={ContactUs} />
        <Route path="/gellary" exact component={Gallery} />
        <Route path="/socialWork" exact component={SocialWork} />
        <Route path="/ourTeam" exact component={OurTeam} />
        <Route path="/package" exact component={Packages} />
      </Switch>
    </Layout>
  );
}

export default App;
