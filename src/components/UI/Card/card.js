import React from "react";
import classes from './card.module.css';
import img from '../../../assets/images/about-ceo.png'
import img1 from '../../../assets/images/tahmid.jpg'
import img3 from '../../../assets/images/ifti.jpg'
const Card = () => {

    return (


        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div className={classes.card} >
                        <div className={classes.imgBx}>
                            <img src={img} alt="images" />
                        </div>
                        <div className={classes.details}>
                            <h2>Sanjana Afrin<br /><span>CEO</span></h2>
                        </div>
                    </div>
                    <div className={classes.card} >
                        <div className={classes.imgBx}>
                            <img src={img1} alt="images" />
                        </div>
                        <div className={classes.details}>
                            <h2>Tahmidul Haque<br /><span>Media Manager</span></h2>
                        </div>
                    </div>
                    <div className={classes.card} >
                        <div className={classes.imgBx}>
                            <img src={img3} alt="images" />
                        </div>
                        <div className={classes.details}>
                            <h2>Ifta Kharul Islam<br /><span>Managing Director</span></h2>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
    )
}
export default Card